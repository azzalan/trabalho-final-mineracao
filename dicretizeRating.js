import broodStandadized from './bases/broodStandadized.json'
import { outputJSON } from './outputJSON.js';

const getDefaultDicretizedProps = () => ({
  radiant_tier_1: false,
  radiant_tier_2: false,
  radiant_tier_3: false,
  radiant_tier_4: false,
  dire_tier_1: false,
  dire_tier_2: false,
  dire_tier_3: false,
  dire_tier_4: false,
})

const getTier = rating => {
  const final = 1.6
  const sub = String((final - rating) * 10)
  const tier_1_8 = Number(sub[0]) + 1
  const tier_1_4 = tier_1_8 % 2 === 0 ? tier_1_8 / 2 : (tier_1_8 + 1) / 2
  return String(tier_1_4)
}

const data = broodStandadized.map(instance => {
  const { radiant_rating, dire_rating } = instance
  delete instance.radiant_rating
  delete instance.dire_rating
  const newInstance = { ...instance, ...getDefaultDicretizedProps() }
  for (let key in newInstance) {
    newInstance[key] = newInstance[key] === 1 ? true : false
  }
  newInstance['radiant_tier_' + getTier(radiant_rating)] = true
  newInstance['dire_tier_' + getTier(dire_rating)] = true
  return newInstance
})

outputJSON(data, 'bases/broodDiscretized')
