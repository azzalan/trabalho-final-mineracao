import { outputJSON } from './outputJSON'
import brood from './brood.json'
import { getOrderProps } from './getOrderProps'

const data = []
brood.map(match => {
  const { team, radiant_win, radiant_rating, dire_rating } = match
  if (radiant_rating && dire_rating)
    data.push({
      brood_in_radiant: team === 0 ? 1 : 0,
      radiant_win: radiant_win ? 1 : 0,
      radiant_rating: radiant_rating / 1000,
      dire_rating: dire_rating / 1000,
      ...getOrderProps(match.ord),
    })
})
outputJSON(data)
