import fs from 'fs'
import uid from 'uuid/v4'

export const outputJSON = (data, name) => {
  const jsonData = JSON.stringify(data)
  const fileName = name ? name : uid()
  fs.writeFile(fileName + '.json', jsonData, () =>
    console.log('Dados arquivados com sucesso. Arquivo: ' + fileName)
  )
}
