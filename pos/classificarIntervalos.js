export const classificarIntervalos = dadosNosIntervalos => {
  const resultadosIntervalos = []
  dadosNosIntervalos.forEach((intervalo, index) => {
    let somaChutes = 0
    let radiantWinCount = 0
    intervalo.forEach(instancia => {
      if (instancia.chute < 0) return
      if (instancia.chute > 1) somaChutes++
      else somaChutes += Math.abs(instancia.chute)
      if (instancia.esperado === 1) radiantWinCount++
      return
    })
    resultadosIntervalos[index] = {
      chuteMedio: somaChutes / intervalo.length,
      radiantWinMedia: radiantWinCount / intervalo.length,
      tamanho: intervalo.length,
    }
  })
  return resultadosIntervalos
}
