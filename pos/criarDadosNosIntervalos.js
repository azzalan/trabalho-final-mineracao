import { outputJSON } from '../outputJSON'
import dados from '../results/neural/r5-50'
import { getIntervalos } from './getIntervalos'
import { classificarIntervalos } from './classificarIntervalos'
import { distribuirDadosNosIntervalos } from './distribuirDadosNosIntervalos'

const quatidadeIntervalos = 8

const intervalos = getIntervalos(quatidadeIntervalos)
const dadosNoIntervalo = distribuirDadosNosIntervalos(dados, intervalos)
const intervalosClassificados = classificarIntervalos(dadosNoIntervalo)

outputJSON(intervalosClassificados, 'pos/neural/r5-50-' + quatidadeIntervalos)
