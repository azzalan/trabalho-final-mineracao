export const distribuirDadosNosIntervalos = (dados, intervalos) => {
  const dadosNoIntervalo = intervalos.map(() => ([]))
  dados.forEach(instancia => {
    const { chute } = instancia
    intervalos.forEach((intervalo, index) => {
      if (chute >= intervalo[0] && chute < intervalo[1]) dadosNoIntervalo[index].push(instancia)
    })
  })
  return dadosNoIntervalo
}
