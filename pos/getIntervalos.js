export const getIntervalos = numIntervalos => {
  const incremento = 1 / numIntervalos
  const inicial = -1
  const final = 2
  const intervalos = []
  let valorInicial = 0
  for (let i = 0; i < numIntervalos; i++) {
    intervalos.push([valorInicial, valorInicial + incremento])
    valorInicial += incremento
  }
  intervalos[0][0] = inicial
  intervalos[numIntervalos - 1][1] = final
  return intervalos
}
